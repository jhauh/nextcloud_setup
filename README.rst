Nextcloud Setup
===============

This repo serves as a guide for spinning up a new
`Nextcloud <https://nextcloud.com/>`_ instance on a fresh Debian-based
server, as well as all the usual hardening to defend against baddies.
Useful for when you want to migrate to another VPS provider.

Note
++++

Please don't hack me if you notice some security gap or issue with this
approach. Consider a Pull/Merge Request instead. Thanks!

Requirements
++++++++++++

You will need:

* **A Linux VPS**: This document assumes Debian-based (apt) but should 
  work with others, just sub in the relevant package manager and keep
  an eye out for some file location differences.
* **A (sub)domain you own**: Strictly speaking you don't need this,
  but skipping it means giving up HTTPS and requires you to ensure your
  IP is static and then remembering it all the time, which is lame.

Steps
+++++

1. Update base image
--------------------
Who knows how old the base image the VPS provider uses is. A quick::

    sudo apt-get update
    sudo apt-get upgrade
    sudo reboot

Should do the trick.

2. Harden ssh access
--------------------

The second you expose your VPS to the whole internet you're gonna get
all kinds of login attempts from all over the place. At this point 
they're already banging at the door. To combat this, we're going to do
a few things:

i. Disable root and password login via ssh
   
   This means you can't ``ssh root@<VPS IP>`` any more, and you must 
   log in with ssh keys instead of passwords. It's better this way, 
   swear.

   Firstly, add your ssh keys to the VPS from whatever local machine
   you're working from. You can ``scp`` them manually like a chump, but
   the better way is to use ``ssh-copy-id``. From your local machine::

     ssh-copy-id -i ~/.ssh/mykey user@<VPS IP>
    
   It'll probably ask you for a password (hopefully for the last time).
   Then, test it by logging in::

     ssh -i ~/.ssh/mykey user@<VPS IP>

   Once you can sucessfully connect passwordlessly, it's time to
   tighten things up. On the VPS, edit ``/etc/ssh/sshd_config``,
   setting the following options::

     PermitRootLogin no
     PasswordAuthentication no

   To make these changes take effect, restart the ssh service::

     sudo systemctl restart ssh

#. Install fail2ban and ufw::

     sudo apt-get install fail2ban ufw
     sudo systemctl start fail2ban
     sudo systemctl enable fail2ban
     sudo ufw enable
     sudo systemctl enable fail2ban

   The enable commands tell systemd to start these services on boot,
   so you don't have to worry about turning them on after every
   reboot. (The above might be overkill)
   
   Fail2ban is pretty set-and-forget, but I like to turn up the 
   ban times and turn down the number of allowed attempts. You can
   copy the ``fail2ban/jail.d/sshd.local`` and
   ``fail2ban/filter.d/sshd.conf`` to the respective folders in 
   ``/etc/fail2ban/`` if you are similarly tough on crime. Then::

     sudo systemctl restart fail2ban

   Configure ufw to deny all incoming connections by default, then 
   selectively allow 80 & 443 (which Nextcloud will use), and 22
   (for connecting via ssh), then restart to make changes take effect::
     
     sudo ufw default deny incoming
     sudo ufw allow 80,443/tcp
     sudo ufw allow 22/tcp
     sudo systemctl restart ufw

#. Optional: Change the port used for ssh

   This one doesn't really harden anything, but it does reduce the
   number of login attempts your server will be spammed with. To 
   accomplish this, we'll need to make changes to some of the files
   we've touched above already. 

   Open up the firewall to your new port (I'll use 2343 as an example)::

     sudo ufw allow 2343/tcp
     sudo systemctl restart ufw

   Update the port which fail2ban listens on in ``jail.d/sshd.local``::

     port=2343

   ..And the restart the fail2ban service. Change the port in 
   ``/etc/ssh/sshd_config``::

     Port 2343

   And restart the service. Log out, and now try to log back in on the
   new port::

     ssh -i ~/.ssh/mykey -p 2343 user@<VPS IP>

   Hopefully this works. The ssh login command is getting a bit
   long-winded, I recommend adding all these server-specific details to
   your ``~/.ssh/config`` so you don't have to remember the port etc.
   in the future::

     Host <VPS IP>
        IdentityFile ~/.ssh/mykey
        Port 2343

   This will mean you can simply ``ssh user@<VPS IP>`` next time, or 
   even ``ssh <VPS IP>`` if your username matches on local and remote.

   Once the new port has been validated as above, it's time to close
   the old one::

     sudo ufw delete allow 22/tcp

   (As always, restart the service). At this point you should have a
   reasonably secure server that you can be confident in.

3. Install Nextcloud
--------------------

There are a number of different approaches to installing Nextcloud on 
Linux; you can do it all by hand, you can use a Docker image, or you
can install a Snap. This guide opts for the latter, for its ease of 
installation. Instructions for all the different installation methods
can be found in the 
`official documentation <https://docs.nextcloud.com/server/18/admin_manual/installation/source_installation.html>`_,
but this guide extends the snap install with some setup and 
configuration suggestions not found in the above link, but instead 
mostly gleaned and adapted from 
`this DO Nextcloud guide <https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-nextcloud-on-ubuntu-18-04>`_.

Firstly, install snap if you don't have it already::

    sudo apt-get install snapd

Then use snap to install Nextcloud::

    sudo snap install nextcloud

This will pull down everything you need to get started. At this point
you can navigate to localhost or 127.0.0.1 in your browser to do
everything via the web interface, but instead we'll use the command 
line::

    sudo nextcloud.manual-install <user> <password>

You may want to prepend a space to the above command so it doesn't get
recorded to your command history.

In order to able to connect to this Nextcloud instance from the outside
world, we'll need to point your (sub)domain at it. You can do this by 
adding a custom resource record to the VPS's IP address inyour domain
registrar's control panel. This varies by registrar but should look 
something like::

    Name: nextcloud (for example, producing nextcloud.domain.com)
    Type: A
    Data: <VPS IP>

Once saved, wait until the change is propogated (how often refreshes
happen also varies by registrar) and you'll be able to reach your
Nextcloud installation via your subdomain URL. In the meantime, we can
let Nextcloud know to accept connections coming from this domain::

    sudo nextcloud.occ config:system:set trusted_domains 1 --value=nextcloud.domain.com

When you can successfully connect this way, it's time to set up SSL::

    sudo nextcloud.enable-https lets-encrypt

You'll be asked for some details. 

4. Harden Nextcloud login
-------------------------

Much like we did for ssh, we can also set fail2ban up to guard from 
spammers trying to brute force guess passwords on the Nextcloud web
app. Copy the Nextcloud jail and filter files from this repo to the 
relevant folders as above, and restart the fail2ban service.

Congratulations
---------------

Wasn't so hard, was it? Enjoy your new Nextcloud instance.


Shortcut
--------
Going through the above steps manually the first time is a good way to
learn what's going on, but if you want to speed things up you can 
clone this repo to the server and run (TODO WIP)::

  bash setup_nextcloud.sh

as sudo and it will run through the above steps and prompt you for
necessary inputs as needed.

To do
-----

If you want to use your VPS for more than just Nextcloud, it can be
useful to go the Docker route. Make that an option here. And maybe the
manual install too, in case you want finer-grained control over
everything.

